
#include "simpletools.h"
#include "smengine.h"
#include "heartbeat.h"

typedef enum
{
  HB_START,
  HB_TIME,
  HB_TIME2,
}HB_ENUM;

typedef struct
{
  HB_ENUM state;
  int time;
} HB_STRUCT;

HB_STRUCT hbs;
SME_STRUCT hb_sme;

/*
 * the main hb task
 */
void HB_Task(void *ptr)
{
  HB_STRUCT *hb;
  
  if(ptr != NULL)
  {
    hb = ptr;
    switch(hb->state)
    {
      case HB_START:
        hb->time = CNT + 10000000;
        hb->state = HB_TIME;
        break;
        
      case HB_TIME:
        if(CNT > hb->time)
        {
          set_output(27,1);
          hb->state = HB_TIME2;
          hb->time = CNT + 10000000;
        }
        break;
        
     case HB_TIME2:
        if(CNT > hb->time)
        {
          set_output(27,0);
          hb->state = HB_TIME;
          hb->time = CNT + 10000000;
        }
        break;
        
      
      default:
        break;
    }        
  }    
}


/*
 *the task initliser
 */
void HB_Task_Init(void *ptr)
{
  HB_STRUCT *hb;
  
  if(ptr != NULL)
  {
    hb = ptr;
    hb->state = HB_START;
    set_direction(27,1);
  }     
}


/******************************************
 * initialise heart beat task
 *****************************************/
void HB_Init(void)
{
  // set ports here
  
  //add to SME task
  SME_AddTask(HB_Task,HB_Task_Init,&hbs,&hb_sme);
  
}
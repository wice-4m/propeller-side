/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/

#include <stdbool.h>
#include "simpletools.h"                      // Include simple tools
#include "fdserial.h"
#include "parallel.h"
#include "smengine.h"
#include "wice.h"


#define TEST_BYTE1  0x5a
#define TEST_BYTE2  0xa5
#define MEM_SIZE    262114
#define MSG_NUMBER   8




typedef struct
{
  unsigned mode:2;    //bits 0 and 1
  unsigned modelatch:1; //bit 2
  unsigned grnled:1;    //bit 3
  unsigned abport:1;    //bit 4
  unsigned bit5:1;      //
  unsigned palclk:1;    //bit 6
  unsigned select:2;    //bit 7
} U30_BITS;

typedef union
{
  U30_BITS b;
  unsigned char uc;
} U30_UNION;

typedef struct
{
  unsigned p0:1;
  unsigned p1:1;
  unsigned p2:1;
  unsigned p3:1;
  unsigned p4:1;
  unsigned p5:1;
  unsigned p6:1;
  unsigned p7:1;
} U4_BITS;

typedef union
{
  U4_BITS b;
  unsigned char uc;
} U4_UNION;


typedef struct
{
  unsigned RAMOE:1;
  unsigned EMUSEL:1;
  unsigned BSEL:1;
  unsigned ENA17:1;
  unsigned ERRCLR:1;
  unsigned ESTATUS:1;
  unsigned LNIBBLE:1;
  unsigned UNIBBLE:1;
} U5_BITS;

typedef union
{
  U5_BITS b;
  unsigned char uc;
} U5_UNION;


typedef struct
{
  unsigned L2D0:1;
  unsigned ENA15:1;
  unsigned EDATA:1;
  unsigned ENCS2:1;
  unsigned ARESET:1;
  unsigned ENA17:1;
  unsigned ENRAM1:1;
  unsigned ENRAM0:1;
} U6_BITS;

typedef union
{
  U6_BITS b;
  unsigned char uc;
} U6_UNION;


typedef struct
{
  LIST_ITEM list;
  CMD_ENUM cmd;
  void *vptr;
} CMD_STRUCT;

typedef enum
{
  WICE_WAIT,
  WICE_CLOSE,
} WICE_ENUM;

typedef struct
{
  WICE_ENUM state;    // current state
  WICE_ENUM rstate;   // return state
  fdserial *fds;    //the serial device
  unsigned int va;  // virtual address
  U30_UNION lf_shadow;  // Virtual LF Buffer
  U4_UNION profile_shadow;  // Virtual profile buffer
  U5_UNION enable_shadow;   // Virtual enable buffer
  U6_UNION control_shadow;  // Virtual control buffer
  LIST_ITEM *alist;  // messages available for use
  LIST_ITEM *wlist;  // Message list
  LIST_ITEM *clist;   // current list
  CMD_STRUCT *cptr;   // pointer current command
} WICE_STRUCT;



CMD_STRUCT cmds[MSG_NUMBER];


/**
 *@brief  allocate memory for SME struct
 */
WICE_STRUCT wice;
SME_STRUCT wice_sme;


/**
 *@brief  set the mode of line feed
 *@param  pointer union
 *@param  mode or address of strobe latch
 */
void wice_lf_mode(U30_UNION *lfptr,int mode)
{
  if(lfptr != NULL)
  {
    lfptr->b.mode = mode;
  }   
}


/**
 *@brief latch U30 with data
 *@parm pointer to struct
 */
void wice_lf_write(U30_UNION *lfptr)
{
  if(lfptr != NULL)
  {
    Parallel_Data(lfptr->uc);  //put the command on data
    Parallel_Linefeed();          //now latch the line feed latch
  }    
}


void wice_strobe_write(void *ptr,int address)
{
  WICE_STRUCT *ws;
  
  if(ptr != NULL)
  {
    ws = ptr; //turn the pointer into structure
      
    wice_lf_mode(&ws->lf_shadow,address);
    wice_lf_write(&ws->lf_shadow);
    switch(address)
    {
      case 0:
        Parallel_Data(ws->profile_shadow.uc);
        break;
        
      case 1:
        Parallel_Data(ws->enable_shadow.uc);
        break;
        
      case 2:
        Parallel_Data(ws->control_shadow.uc);
        break;
        
      default:
        Parallel_Data(0);
        break;
    }
    Parallel_Strobe();
  }
}

/**
 *@brief  set the adddress to zero
 *@param  Pointer to struct
 */ 
void wice_reset_address(void *ptr)
{
  WICE_STRUCT *ws;
  
  if(ptr != NULL)
  {
    ws = ptr;
    
    ws->va = 0; //address now zero
    ws->control_shadow.b.ARESET = true;
    wice_strobe_write(ws,2);
    ws->control_shadow.b.ARESET = false;
    wice_strobe_write(ws,2);
  }
}



/**
 *@brief  increment the address and virtual addres
 *@param  pointer
 */
void wice_increment_address(void *ptr)
{
  WICE_STRUCT *ws;
  
  if(ptr != NULL)
  {
    ws = ptr;
    ws->va++;
    Parallel_Init_Line();
  }   
}

/**
 *@brief  Set the profile latch to default
 *@param  pointer profile shadow
 */
void wice_profile_default(U4_UNION *pfptr)
{
  if(pfptr != NULL)
  {
    pfptr->uc = 0xff;
 }
}

/**
 *@brief  Set the enable latch to default
 *@param  pointer enable shadow
 */
void wice_enable_default(U5_UNION *esptr)
{
  if(esptr != NULL)
  {
    esptr->uc = 0xc7;
 }
}

/**
 *@brief  Set the control latch to default
 *@param  pointer control shadow
 */
void wice_control_default(U6_UNION *csptr)
{
  if(csptr != NULL)
  {
    csptr->uc = 0x00;
    csptr->b.L2D0 = true;
 }
} 

/**
 *@brief  Reset the address counter
 *@param  latch high for reset low clear increment
 */
void wice_control_arst(U6_UNION *csptr,bool state)
{
  if(csptr != NULL)
  {
    csptr->b.ARESET = state;
 }
}



/**
 *@brief  selects output A or B
 *@param  Pointer lf shadow
 *        mode true port b false port a
 */
void wice_select(U30_UNION *lfptr,bool mode)
{
  if(lfptr != NULL)
  {
    lfptr->b.abport = mode;
    lfptr->b.modelatch = false; 
    lfptr->b.grnled = true; //led on
    lfptr->b.palclk = true;
    lfptr->b.select = true;
    wice_lf_write(lfptr);
    lfptr->b.modelatch = true;
    wice_lf_write(lfptr);
    lfptr->b.select = false; //turn off select      
  }     
}

void wice_lf_disable(U30_UNION *lfptr)
{
  if(lfptr != NULL)
  {
    lfptr->b.palclk = true;
    lfptr->b.modelatch = true;
    wice_lf_mode(lfptr,3);
    lfptr->b.abport = false;
    wice_lf_write(lfptr);
  }
}





unsigned char wice_read_status(void)
{
  unsigned retval = 0;
  
  retval |= Parallel_Read_Select();
  retval <<= 1;
  retval |= Parallel_Read_Pend();
  retval <<= 1;
  retval |= Parallel_Read_Ack();
  retval <<= 1;
  retval |= Parallel_Read_Busy();
  return(retval);
}

unsigned char wice_read_memory(void *ptr)
{
  WICE_STRUCT *ws;
    
  unsigned char retval = 0;
  if(ptr != NULL)
  {
    ws = ptr;
    ws->enable_shadow.b.RAMOE = false;  //enable ram output enable
    ws->enable_shadow.b.UNIBBLE = false;  // turn on upper nibble
    wice_strobe_write(ws,1);
    retval |= wice_read_status();
    retval <<= 4;
    ws->enable_shadow.b.UNIBBLE = true;
    ws->enable_shadow.b.LNIBBLE = false;
    wice_strobe_write(ws,1);
    retval |= wice_read_status();
    ws->enable_shadow.b.LNIBBLE = true;
    ws->enable_shadow.b.RAMOE = true;
    wice_strobe_write(ws,1);
  }
  return(retval);
}  

void wice_write_memory(unsigned char data)
{
  Parallel_Data(data);
  Parallel_Select_In();
}

   
bool wice_test_memory(void *ptr)
{
  WICE_STRUCT *ws;
  bool retval = false;
  bool ra,rb;
  unsigned char rc;
    
  if(ptr != NULL)
  {
    ws = ptr;
    ra = false;
    rb = false;
      
    wice_write_memory(TEST_BYTE1);
    rc = wice_read_memory(ws);
    if(rc == TEST_BYTE1) ra = true;
    wice_increment_address(ws);
    wice_write_memory(TEST_BYTE2);
    rc = wice_read_memory(ws);
    if(rc == TEST_BYTE2) rb = true;
    retval = ra == rb;
   }
  return(retval);
}

void wice_fill_memory(unsigned char fill)
{
  int count;
  
  count = MEM_SIZE;
  while(count)
  {
    wice_write_memory(fill);
    Parallel_Init_Line();
    count--;
  }
}

/**
 *@brief  write a command to run task
 *@param  cmd
 *@param  command parameter
 */
bool wice_command(CMD_ENUM cmd, void *ptr)
{
  bool retval = false;
  LIST_ITEM *tl;
  CMD_STRUCT *cptr;
  
  if(wice.alist != NULL)
  {
    tl = wice.alist;
    wice.alist = List_RemoveItem(wice.alist,tl);
    
    if(tl->owner != NULL)
    {
      cptr = tl->owner;
      cptr->cmd = cmd;
      cptr->vptr = ptr;
      wice.wlist = List_AddItem(wice.wlist,tl);
      retval = true;
    }      
  }
  return(retval);
}


/**
 *@brief  Main task wice
 *@param  Pointer to State struct
 */
void WICE_Task(void *ptr)
{
  WICE_STRUCT *ws;
  
  if(ptr != NULL)
  {
    ws = ptr;
    //writeLine(ws->fds,"WICE Driver");
    switch(ws->state)
    {
      case WICE_WAIT:
        if(ws->wlist != NULL)
        {
          ws->clist = ws->wlist;
          ws->wlist = List_RemoveItem(ws->wlist,ws->clist);
          if( ws->clist->owner != NULL)
          {
            ws->cptr = ws->clist->owner;
            ws->state = WICE_CLOSE;
            ws->rstate = WICE_WAIT;
          }
          else
          {
            ws->state = WICE_CLOSE;
            ws->rstate = WICE_WAIT;
          }                              
        }         
        break;
        
       //Close 
       case WICE_CLOSE:
        if(ws->clist != NULL)
        {
          ws->alist = List_AddItem(ws->alist,ws->clist);
          ws->clist = NULL;
        }
        else
        {          
          ws->state = ws->rstate;
        }          
        break;
        
      default:
        break;
    }
  }         
}


/**
 *@brief  Start Task for wice
 *@param  Pointer to state memory
 */
void WICE_Task_Start(void *ptr)
{
  int i;
  WICE_STRUCT *ws;
  
  if(ptr != NULL)
  {
    ws = ptr;
    
    ws->state = WICE_WAIT; 
    ws->wlist = NULL;
    ws->alist = NULL;
    ws->lf_shadow.uc = 0;
    wice_lf_disable(&ws->lf_shadow);
    wice_select(&ws->lf_shadow,0);  //port a
    wice_profile_default(&ws->profile_shadow);
    wice_strobe_write(ws,0);
    wice_enable_default(&ws->enable_shadow);
    wice_strobe_write(ws,1);
    wice_control_default(&ws->control_shadow);
    wice_control_arst(&ws->control_shadow,1);   
    wice_strobe_write(ws,2);
    wice_control_arst(&ws->control_shadow,0);  //truns off address reset
    wice_strobe_write(ws,2);
    wice_read_memory(ws);
    wice_increment_address(ws);
    wice_reset_address(ws);
    wice_test_memory(ws);     //test to see if we can read and wirte
    wice_reset_address(ws);
    
    for(i=0; i<MSG_NUMBER; i++)
    {
      cmds[i].list.owner = &cmds[i];
      ws->alist = List_AddItem(ws->alist, &cmds[i].list);
    }    
  }    
}



/**
 *@brief initilaise wice driver
 *@param  pointer to serial driver
 */
void WICE_Init(fdserial *fds)
{
  
  if(fds != NULL)
  {
    wice.fds = fds;
    SME_AddTask(WICE_Task,WICE_Task_Start,&wice,&wice_sme);
    
    writeLine(fds,"WICE Driver");
  }  
  
}

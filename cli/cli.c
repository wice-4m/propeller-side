

#include <stdbool.h>
#include "simpletools.h"                   // Include simple tools
//#include "simpletext.h"
#include "fdserial.h"
#include "parallel.h"
#include "smengine.h"
#include "cli.h"


#define CMD_SIZE  32
#define CLINUMBER 32


typedef enum
{
  CLI_START,
  CLI_GETC,
  CLI_PROCESS,
  CLI_DISPLAY,
} CLI_ENUM;

typedef struct
{
  LIST_ITEM list;
  char cmd[CMD_SIZE];
} CMD_STRUCT;

typedef struct
{
  CLI_ENUM state;     // state of current task
  int c;
  int count;
  int number;
  bool echo;
  fdserial *fds;      // current serial driver
  char *obuffer;      // buffer to hold data
  char *tbuffer;      // temp buffer
  LIST_ITEM *clist;   // curent list
  LIST_ITEM *dlist;
  LIST_ITEM *alist;
  CMD_STRUCT *cs;
} CLI_STRUCT;




// allocate some memory
CLI_STRUCT cli_memory;
SME_STRUCT cli_sme;
CMD_STRUCT climem[CLINUMBER];




/**
 *@brief routine to read keyboard data
 *@param  poiter CLI struct
 *@return new state
 */
CLI_ENUM CLI_Gets(CLI_STRUCT *cli)
{
  CLI_ENUM retval;
  CMD_STRUCT *ct = NULL;
  int c;
  
  if(cli != NULL)
  {
    retval = cli->state; //save the original state
    
    if(cli->clist == NULL)
    {
      cli->clist = cli->alist;
      cli->alist = List_RemoveItem(cli->alist,cli->clist);
      ct = cli->clist->owner;
      cli->obuffer = ct->cmd;
      cli->tbuffer = cli->obuffer;
      cli->count = 0;
    }
                   
    if(cli->count < CMD_SIZE)
    {
      //c = fdserial_rxChar(cli->fds);
      c = fdserial_rxCheck(cli->fds);
      if(c >= 0 ) // any minus value is no good
      {
        cli->count++;
        if(c == BKSP || c == 127) //del and back space
        {
          if(cli->tbuffer > cli->obuffer)
          {
            if(cli->echo)
            {
              fdserial_txChar(cli->fds,'\010');
              fdserial_txChar(cli->fds,' ');
              fdserial_txChar(cli->fds,'\010');
            }                  
            cli->count--;
            cli->tbuffer--;
          }
          cli->count--;                
        }
        else //e1
        {
          if(cli->echo) fdserial_txChar(cli->fds,c);
          if(c == '\r')
          {
            if(cli->echo) fdserial_txChar(cli->fds,'\n');
          }
                                           
          if(c == '\r' || c == '\n')
          {
            //writeHex(cli->fds,c);
            *(cli->tbuffer) = 0;
            cli->dlist = List_AddItem(cli->dlist,cli->clist);
            cli->clist = NULL;
            retval = CLI_PROCESS;
          }
          else if(c == ' ')
          {
            *(cli->tbuffer) = 0;
            cli->dlist = List_AddItem(cli->dlist,cli->clist);
            cli->clist = NULL;
          }
          else
          {
            //writeHex(cli->fds,c);
            *(cli->tbuffer++) = c;
          }                           
        } //e1
      }            
    }
    else
    {
      *(cli->tbuffer-1) = 0;
      cli->dlist = List_AddItem(cli->dlist,cli->clist);
      cli->clist = NULL;
      retval = CLI_PROCESS;
    }                   
  }
  return(retval);
}  

/**
 * @brief Test string to see if all is hex
 * @param Null terminated string
 * @return 0 whole string 
 *          1 string is hex number
 */
bool CLI_Hex_String(CLI_STRUCT *cli)
{
  bool retval = false;
  bool exit = false;
  int size;
  int count = 0;
  LIST_ITEM *tl;
  
  if(cli != NULL)
  {

    size = strlen(cli->obuffer);
    cli->tbuffer = cli->obuffer;
    while(*(cli->tbuffer))
    {
      //writeHex(cli->fds,*(cli->tbuffer));
      if(isxdigit(*(cli->tbuffer)))
      {
        //writeHex(cli->fds,cli->count);
        count++;
      }
      cli->tbuffer++;
    }
    writeStr(cli->fds,"c:"); writeDec(cli->fds,count); writeChar(cli->fds,'\r');
    writeStr(cli->fds,"s:"); writeDec(cli->fds,size); writeChar(cli->fds,'\r'); 
    
    retval = (count == size);
  }    
  return(retval);
}
  

/**
 *@brief Command Line Interface Task
 *@param pointer task struct
 */
void CLI_Task(void *ptr)
{
  CLI_STRUCT *cli;
  LIST_ITEM *tl;
  
  if(ptr != NULL)
  {
    cli = ptr;

    //writeHex(cli->fds,cli->state);
    //writeChar(cli->fds,'\r');
    //fdserial_rxChar(cli->fds);
    switch(cli->state)
    {
      case CLI_START:
        cli->state = CLI_GETC;
        cli->count = 0;
        cli->tbuffer = cli->obuffer;
        break;
      
      case CLI_GETC:  // read charcters into buffer
        //writeHex(cli->fds,cli->count);
        cli->state = CLI_Gets(cli);
        //writeHex(cli->fds,cli->state);
        //writeChar(cli->fds,'\r');
        break;
        
      case CLI_PROCESS:   //lets see we have number or function
        if(cli->dlist != NULL)
        {
          tl = cli->dlist;
          cli->dlist = List_RemoveItem(cli->dlist,tl);
          cli->cs = tl->owner;
          if( cli->cs != NULL)
          {
            writeStr(cli->fds,cli->cs->cmd);
            //if(!CLI_Hex_String(cli))
            //{
            //  writeline(cli->fds,"cmd");
            // }
            // else
            // {
            //   _scanf_getl(cli->obuffer,&cli->number,16,5,0);
            //   writeHex(cli->fds, cli->number);
             //  writeChar(cli->fds,'\r');
            // }
            //return the working list back to avalable list
            cli->alist = List_AddItem(cli->alist,tl); 
           }
           else
           {
             cli->state = CLI_START;
           }                         
        }
        else
        {                   
          cli->state = CLI_START;
        }        
        break;
        
      default:

        cli->state = CLI_START;
        break;
    }      
  }    
}  


/**
 * @brief initialise the task
 * @param pointer to task structure
 */
void CLI_Task_Start(void *ptr)
{
  CLI_STRUCT *cli;
  int i;
  
  
  if(ptr != NULL)
  {
    cli = ptr;
    cli->state = CLI_START;
    cli->echo = true;
    cli->obuffer = NULL;
    cli->clist = NULL;
    cli->dlist = NULL;
    cli->alist = NULL;
    for(i=0; i<CLINUMBER;i++)
    {
      climem[i].list.owner = &climem[i];
      cli->alist = List_AddItem(cli->alist,&climem[i].list);
    }              
  }    
}
  
void CLI_Init(fdserial *fds)
{
 
  if(fds != NULL)
  {
    cli_memory.fds = fds;
    SME_AddTask(CLI_Task,CLI_Task_Start,&cli_memory,&cli_sme);
    writeLine(fds,"CLI Driver");
  }    
}
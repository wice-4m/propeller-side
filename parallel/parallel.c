/**
 *api to parallel port
 */

#include <stdbool.h>
#include "simpletools.h"           // Include simple tools
#include "list.h"
#include "smengine.h"
#include "fdserial.h"
#include "simpletext.h"
#include "parallel.h"

#define HIGH          true
#define LOW           false
#define U2_OE         17
#define U2_DIR        23
#define U2_DATA_HIGH  7
#define U2_DATA_LOW   0
#define U3_DIR        18
#define U3_STROBE     8
#define U3_LINEFEED   9
#define U4_DIR        19
#define U4_INIT       10
#define U4_SELIN      11
#define U5_DIR        20
#define U5_ACK        12
#define U5_BUSY       13
#define U6_DIR        21
#define U6_PEND       14
#define U6_SELECT     15
#define U7_DIR        22
#define U7_ERROR      16


bool Parallel_Read_Select(void)
{
  return(get_state(U6_SELECT));
}

bool Parallel_Read_Pend(void)
{
  return(get_state(U6_PEND));
}

bool Parallel_Read_Ack(void)
{
  return(get_state(U5_ACK));
}

bool Parallel_Read_Busy(void)
{
  return(get_state(U5_BUSY));
}

/**
 *@brief toggle linfeed signal
 */
void Parallel_Linefeed(void)
{
  set_output(U3_LINEFEED,LOW);
  set_output(U3_LINEFEED,HIGH);
  set_output(U3_LINEFEED,LOW);
}


/**
 *@brief toggle linfeed signal
 */
void Parallel_Init_Line(void)
{
  set_output(U4_INIT,LOW);
  set_output(U4_INIT,HIGH);
  set_output(U4_INIT,LOW);
}


/**
 *@brief toggle linfeed signal
 */
void Parallel_Select_In(void)
{
  set_output(U4_SELIN,LOW);
  set_output(U4_SELIN,HIGH);
  set_output(U4_SELIN,LOW);
}

/**
 *@brief  put data on to the data bus
 *@parm   8 bit data to be placed on data bus
 */
void Parallel_Data(unsigned char data)
{
  set_outputs(7,0,data);  //send data
}

/**
 *@brief  Toggle the strobe line
 */
void Parallel_Strobe(void)
{
  set_output(U3_STROBE,LOW);
  set_output(U3_STROBE,HIGH);
  set_output(U3_STROBE,LOW);
}
/**
 *@brief Start up the parallel Driver
 */
void Parallel_Init(fdserial *fds)
{
  if(fds != NULL)
  {
    writeLine(fds,"Parallel Driver");
    //U2 Setup
    set_output(U2_OE,HIGH);  //set pin High bus is tri state
    set_direction(U2_OE,true);  //now pin is output
    set_outputs(U2_DATA_HIGH,U2_DATA_LOW,0xff); //now safe 
    set_output(U2_DIR,LOW);
    set_direction(U2_DIR,true);   //output
    set_directions(U2_DATA_HIGH,U2_DATA_LOW,0xff); //now output
    set_outputs(U2_DATA_HIGH,U2_DATA_LOW,0xff);
    set_output(U2_OE,LOW);
    writeLine(fds,"Parallel U2 Setup");
    
    //U3 Setup
    set_output(U3_DIR,LOW);
    set_direction(U3_DIR,true);
    set_outputs(U3_LINEFEED,U3_STROBE,0);
    set_directions(U3_LINEFEED,U3_STROBE,3);
    writeLine(fds,"Parallel U3 Setup");
  
    //U4 Setup
    set_output(U4_DIR,LOW);
    set_direction(U4_DIR,true);
    set_outputs(U4_SELIN,U4_INIT,LOW);
    set_directions(U4_SELIN,U4_INIT,3);
    writeLine(fds,"Parallel U4 Setup");
  
    //U5 setup
    set_output(U5_DIR,HIGH);
    set_direction(U5_DIR,true);
    set_directions(U5_BUSY,U5_ACK,0);
    writeLine(fds,"Parallel U5 Setup");
  
    //U6 setup
    set_output(U6_DIR,HIGH);
    set_direction(U6_DIR,true);
    set_directions(U6_SELECT,U6_PEND,0);
    writeLine(fds,"Parallel U6 Setup");
  
    //U7 setup
    set_output(U7_DIR,HIGH);
    set_direction(U7_DIR,true);
    set_direction(U7_ERROR,false);
    writeLine(fds,"Parallel U7 Setup");
    
  }    
}
#ifndef PARALLEL_H
#define PARALLEL_H

extern void Parallel_Init(fdserial *pds);
extern void Parallel_Data(unsigned char data);
extern void Parallel_Linefeed(void);
extern void Parallel_Strobe(void);
extern bool Parallel_Read_Select(void);
extern bool Parallel_Read_Pend(void);
extern bool Parallel_Read_Ack(void);
extern bool Parallel_Read_Busy(void);
extern void Parallel_Init_Line(void);
extern void Parallel_Select_In(void);

#endif

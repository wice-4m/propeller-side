/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple tools
#include "list.h"
#include "smengine.h"
#include "fdserial.h"
#include "simpletext.h"
#include "heartbeat.h"
#include "parallel.h"
#include "wice.h"
#include "cli.h"

fdserial *fds;

int main()                                    // Main function
{
  // Add startup code here.
  fds = fdserial_open(31,30,0,115200);
  writeStr(fds,"Serial Opened $");
  writeHex(fds,(int)fds);
  writeChar(fds,'\r');
  SME_Init();
  writeLine(fds,"SME Initilised");
  HB_Init();  //start heartbeat
  writeLine(fds,"Heart beat initilised");
 
  Parallel_Init(fds);
  WICE_Init(fds);
  CLI_Init(fds);
  
  while(1)
  {
    // Add main loop code here.
    SME_Execute();
    
  }  
}
